# alpine-xrdp
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-xrdp)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-xrdp)

### x64
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721/alpine-xrdp/x64)
### aarch64
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721/alpine-xrdp/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : xrdp
    - An open source remote desktop protocol(rdp) server. 



----------------------------------------
#### Run

```sh
docker run -d -t \
        forumi0721/alpine-xrdp:[ARCH_TAG]
```



----------------------------------------
#### Usage

* RDP address : localhost:3389



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 3389/tcp           | RDP Port                                         |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

